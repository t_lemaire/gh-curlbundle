<?php

namespace GorillaHub\CurlBundle\Interfaces;

use GorillaHub\CurlBundle\Entity\CurlStatistics;

interface CurlObserver
{
	/**
	 * @param CurlStatistics $curlStatistics
	 * @return mixed
	 */
	public function log(CurlStatistics $curlStatistics);

    /**
     * @param float $loggingRate
     */
	public function setLoggingRate($loggingRate);

    /**
     * @return int
     */
    public function getLoggingRate();
}
