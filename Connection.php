<?php

namespace GorillaHub\CurlBundle;

use GorillaHub\CurlBundle\Entity\CurlStatistics;
use GorillaHub\CurlBundle\Interfaces\CurlObserver;


class Connection
{

	/**
	 * @var resource
	 */
	protected $curl = null;

	/**
	 * @var string
	 */
	protected $error;

	/**
	 * @var int
	 */
	protected $errorNo;

	/**
	 * @var string
	 */
	protected $info;

	/**
	 * @var array
	 */
	protected $options = array();

	/**
	 * @var string
	 */
	protected $userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0';

	/**
	 * @var int|null The HTTP status of the last call, or null if none.
	 */
	protected $httpStatus;

	/**
	 * @var array
	 */
	protected $observers = array();

	public function __construct()
	{
		$this->options = array(
			CURLOPT_CONNECTTIMEOUT => 5,
			CURLOPT_TIMEOUT => 5,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERAGENT => $this->userAgent
		);
	}

	public function __destruct()
	{
		$this->close();
	}

	/**
	 * @param $option
	 * @param $value
	 */
	public function setOption($option, $value)
	{
		$this->options[$option] = $value;
	}

	/**
	 * @param array $options
	 *
	 * @return $this
	 */
	public function setOptions(array $options)
	{
		$this->options = $this->mergeOptions($this->options, $options);
		return $this;
	}

	/**
	 * @param string $userAgent
	 */
	public function setUserAgent($userAgent)
	{
		$this->userAgent = $userAgent;
	}

	/**
	 * @return string
	 */
	public function getUserAgent()
	{
		return $this->userAgent;
	}

	/**
     * In order to pass the stats to the Observer it has to be used with the close() function
	 * @return boolean|string
	 */
	public function exec()
	{
		$this->curl = \curl_init();
		\curl_setopt_array($this->curl, $this->options);

		$return = \curl_exec($this->curl);
        $this->error = \curl_error($this->curl);
        $this->info = \curl_getinfo($this->curl);
        $this->errorNo = \curl_errno($this->curl);

        $httpStatus = \curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        $this->httpStatus = ($httpStatus !== false) ? (int)$httpStatus : null;
        $this->notifyObservers();

        return $return;
	}

	/**
	 * @return array|string
	 */
	public function getInfo()
	{
		return $this->info;
	}

	/**
	 * @return bool
	 */
	public function isSuccess()
	{
		return !$this->errorNo;
	}

	/**
	 * @return string
	 */
	public function getError()
	{
		return $this->error;
	}

	/**
	 * @return int
	 */
	public function getErrorNo()
	{
		return $this->errorNo;
	}

	/**
	 * @return int|null {@see $httpStatus}
	 */
	public function getHttpStatus()
	{
		return $this->httpStatus;
	}

	/**
	 * Close curl handle
	 */
	public function close()
	{
		if ($this->curl !== null) {
			\curl_close($this->curl);
			$this->curl = null;
		}
	}

	/**
	 * @param string $url
	 * @param array $options
	 *
	 * @return bool|string
	 */
	public function get($url, array $options = array())
	{
		unset($this->options[CURLOPT_POST]);
		unset($this->options[CURLOPT_POSTFIELDS]);
		$this->setOptions($this->mergeOptions(array(
			CURLOPT_URL => $url
		), $options));

		$content = $this->exec();
		$this->close();
		return $content;
	}

	/**
	 * @param string $url
	 * @param string $data
	 * @param array $options
	 *
	 * @return bool|string
	 */
	public function post($url, $data, array $options = array())
	{
		$this->setOptions($this->mergeOptions(array(
			CURLOPT_URL => $url,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data
		), $options));

		$content = $this->exec();
		$this->close();
		return $content;
	}

	/**
	 * @param string $url
	 * @param string $pathToFile
	 * @param array $data
	 * @param array $options
	 *
	 * @return bool|string
	 * @throws \Exception
	 */
	public function postFile($url, $pathToFile, array $data, array $options = array())
	{
		if (!is_file($pathToFile)) {
			throw new \Exception('File not found at location: ' . $pathToFile);
		}

		$curlFile = new \CURLFile(
		    $pathToFile,
            mime_content_type($pathToFile),
			pathinfo($pathToFile, PATHINFO_BASENAME)
        );

		$this->setOptions($this->mergeOptions(array(
			CURLOPT_URL => $url,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array_merge(array('file' => $curlFile), $data)

		), $options));

		$content = $this->exec();
		$this->close();
		return $content;
	}

	/**
	 * @param string $url
	 * @param string $data
	 * @param array $options
	 *
	 * @return bool|string
	 */
	public function put($url, $data, array $options = array())
	{
		$this->setOptions($this->mergeOptions(array(
			CURLOPT_URL => $url,
			CURLOPT_CUSTOMREQUEST => 'PUT',
			CURLOPT_POSTFIELDS => $data
		), $options));

		$content = $this->exec();
		$this->close();

		return $content;
	}

	/**
	 * @param string $url
	 * @param string $data
	 * @param array $options
	 *
	 * @return bool|string
	 */
	public function delete($url, $data, array $options = array())
	{
		$this->setOptions($this->mergeOptions(array(
			CURLOPT_URL => $url,
			CURLOPT_CUSTOMREQUEST => 'DELETE',
			CURLOPT_POSTFIELDS => $data
		), $options));

		$content = $this->exec();
		$this->close();

		return $content;
	}

	/**
	 * In a perfect world (ie without PHP), this would not exist but since array_merge loses numeric indexes,
	 * we're stuck with this...
	 *
	 * @param array $default_options
	 * @param array $options
	 *
	 * @return array
	 */
	private function mergeOptions(array $default_options, array $options)
	{
		$merged = $default_options;

		foreach ($options as $key => $value) {

			$merged[$key] = $value;
		}

		return $merged;
	}

    /**
     * Adds an observer to the observer array
     * @param CurlObserver $curlObserver
     */
    public function registerObserver(CurlObserver $curlObserver)
    {
        array_push($this->observers, $curlObserver);
    }

	/**
	 * Removes an observer from the observers array
	 * @param CurlObserver $curlObserver
	 *
	 */
	public function removeObserver(CurlObserver $curlObserver)
	{
		$key = array_search($curlObserver, $this->observers, true);
		if ($key !== false) {
			unset($this->observers[$key]);
		}
	}

	/**
	 * Calls log() from the Observer in order to register the statistics
	 */
	public function notifyObservers()
	{
		foreach ($this->observers as $observer) {
            $observer->log(new CurlStatistics($this->getInfo()));
		}
	}

}
