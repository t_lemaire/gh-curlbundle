<?php
namespace GorillaHub\CurlBundle\Mocks;

/**
 * Details about a response to send to the SUT when it uses MockConnection.
 */
class ConnectionTestCaseResponse
{
	/** @var string The curl error message to return (an empty string means none). */
	public $error = '';

	/** @var int The curl error number to return (0 means no error). */
	public $errorNo = 0;

	/** @var mixed[] The curl info, as returned by curl_getinfo. */
	public $info = [
		'http_code' => '200'
	];

	/** @var string The request body to return, or false to simulate a failure. */
	public $body;

}