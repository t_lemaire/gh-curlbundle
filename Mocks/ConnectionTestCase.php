<?php
namespace GorillaHub\CurlBundle\Mocks;

/**
 * An instance of this class is used to define a single call that should be simulated by MockConnection.  If the
 * $url and $method of the call matches, this object is used to define the details of the response and
 */
class ConnectionTestCase
{
	/** @var string The URL for which to return the simulated response. */
	public $url;

	/** @var string The method for which to return the simulated response, e.g. 'POST', 'GET', 'PUT'. */
	public $method;

	/**
	 * @var ConnectionTestCaseRequest|null Details about the request made by the SUT, or null if the SUT has not yet
	 * 		made a curl call matching this test case.
	 */
	public $request = null;

	/** @var ConnectionTestCaseResponse Details about the response to send to the SUT when it uses MockConnection. */
	public $response;

	public function __construct() {
		$this->response = new ConnectionTestCaseResponse();
	}

	public function __clone() {
		$this->request = clone $this->request;
		$this->response = clone $this->response;
	}

}
