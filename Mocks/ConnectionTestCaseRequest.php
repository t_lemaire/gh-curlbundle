<?php
namespace GorillaHub\CurlBundle\Mocks;

/**
 * Details about a request made by the SUT.
 */
class ConnectionTestCaseRequest
{
	/** @var string[] The post fields sent by the user, or an empty array if none. */
	public $postFields = [];

	/** @var string[] The query parameters passed by the user, or an empty array if none. */
	public $queryParams = [];
}