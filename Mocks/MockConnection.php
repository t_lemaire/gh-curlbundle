<?php

namespace GorillaHub\CurlBundle\Mocks;


use GorillaHub\CurlBundle\Connection;

class MockConnection extends Connection
{
	/** @var ConnectionTestCase */
	private $lastTestCase;

	/** @var ConnectionTestCase[] */
	private $testCases;

	/**
	 * @param ConnectionTestCase[] $connectionTestCases All of the HTTP requests that should be simulated.  If the
	 *		SUT calls the same URL and method multiple times, then on the nth such call, the nth matching test case
	 * 		is used.
	 */
	public function __construct($connectionTestCases)
	{
		$this->testCases = $connectionTestCases;
	}

	public function __destruct()
	{
	}

	/**
	 * @inheritdoc
	 */
	public function exec()
	{
		$method = $this->getMethod();
		$urlWithoutParams = $this->getUrlWithoutParams();
		foreach ($this->testCases as $key => $testCase) {
			if (strcasecmp($testCase->method, $method) == 0 && $testCase->url === $urlWithoutParams) {
				$this->lastTestCase = $testCase;
				$testCase->request = new ConnectionTestCaseRequest();
				if (isset($this->options[CURLOPT_POSTFIELDS])) {
					parse_str($this->options[CURLOPT_POSTFIELDS], $testCase->request->postFields);
				}
				$testCase->request->queryParams = $this->getQueryParams();
				unset($this->testCases[$key]);
				return $testCase->response->body;
			}
		}
		throw new \Exception("Test case for " . $method . ' ' . $urlWithoutParams . ' not found.');
	}

	/**
	 * @inheritdoc
	 */
	public function close()
	{
		$this->error = $this->lastTestCase->response->error;
		$this->info = $this->lastTestCase->response->info;
		$this->errorNo = $this->lastTestCase->response->errorNo;
		$httpStatus = $this->lastTestCase->response->info['http_code'];
		$this->httpStatus = ($httpStatus !== false) ? (int)$httpStatus : null;
	}

	private function getMethod() {
		if (isset($this->options[CURLOPT_POST]) && $this->options[CURLOPT_POST]) {
			return 'POST';
		}
		if (isset($this->options[CURLOPT_CUSTOMREQUEST])) {
			return strtoupper($this->options[CURLOPT_CUSTOMREQUEST]);
		}
		return 'GET';
	}

	private function getUrlWithoutParams() {
		$url = $this->options[CURLOPT_URL];
		return (strpos($url, '?') !== false)
				? substr($url, 0, strpos($url, '?'))
				: $url;
	}

	private function getQueryParams() {
		$url = $this->options[CURLOPT_URL];
		$urlQuery = parse_url($url, PHP_URL_QUERY);
		if (!is_string($urlQuery)) {
			return [];
		}
		parse_str($urlQuery, $queryParams);
		return $queryParams;
	}
}
