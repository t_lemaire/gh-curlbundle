<?php

namespace GorillaHub\CurlBundle\Entity;

use DateTimeImmutable;

class CurlStatistics
{
	/**
	 * @var string
	 */
	protected $url;
	/**
	 * @var string
	 */
	protected $contentType;
	/**
	 * @var int
	 */
	protected $httpCode;
	/**
	 * @var int
	 */
	protected $headerSize;
	/**
	 * @var int
	 */
	protected $requestSize;
	/**
	 * @var int
	 */
	protected $fileTime;
	/**
	 * @var int
	 */
	protected $sslVerifiedResult;
	/**
	 * @var int
	 */
	protected $redirectCount;
	/**
	 * @var float
	 */
	protected $totalTime;
	/**
	 * @var float
	 */
	protected $nameLookupTime;
	/**
	 * @var float
	 */
	protected $connectTime;
	/**
	 * @var float
	 */
	protected $pretransferTime;
	/**
	 * @var float
	 */
	protected $sizeUpload;
	/**
	 * @var float
	 */
	protected $sizeDownload;
	/**
	 * @var float
	 */
	protected $speedDownload;
	/**
	 * @var float
	 */
	protected $speedUpload;
	/**
	 * @var float
	 */
	protected $downloadContentLength;
	/**
	 * @var float
	 */
	protected $uploadContentLength;
	/**
	 * @var float
	 */
	protected $startTransferTime;
	/**
	 * @var float
	 */
	protected $redirectTime;
	/**
	 * @var string
	 */
	protected $redirectUrl;
	/**
	 * @var string
	 */
	protected $primaryIp;

	/**
	 * @var array
	 */
	protected $certInfo;
	/**
	 * @var int
	 */
	protected $primaryPort;
	/**
	 * @var string
	 */
	protected $localIp;
	/**
	 * @var int
	 */
	protected $localPort;
    /**
     * @var string
     */
    protected $timestampOfCurl;

    /**
     *
     * @param array $info
     * @throws \Exception
     */
	public function __construct(array $info) {
        $this->timestampOfCurl = new DateTimeImmutable();
		$this->url = $info['url'];
		$this->contentType = $info['content_type'];
		$this->httpCode = $info['http_code'];
		$this->headerSize = $info['header_size'];
		$this->requestSize = $info['request_size'];
		$this->fileTime = $info['filetime'];
		$this->sslVerifiedResult = $info['ssl_verify_result'];
		$this->redirectCount = $info['redirect_count'];
		$this->totalTime = $info['total_time'];
		$this->nameLookupTime = $info['namelookup_time'];
		$this->connectTime = $info['connect_time'];
		$this->pretransferTime = $info['pretransfer_time'];
		$this->sizeUpload = $info['size_upload'];
		$this->sizeDownload = $info['size_download'];
		$this->speedDownload = $info['speed_download'];
		$this->speedUpload = $info['speed_upload'];
		$this->downloadContentLength = $info['download_content_length'];
		$this->uploadContentLength = $info['upload_content_length'];
		$this->startTransferTime = $info['starttransfer_time'];
		$this->redirectTime = $info['redirect_time'];
		$this->redirectUrl = $info['redirect_url'];
		$this->primaryIp = $info['primary_ip'];
		//need to think how to work with this one
		$this->certInfo = $info['certinfo'];
		$this->primaryPort = $info['primary_port'];
		$this->localIp = $info['local_ip'];
		$this->localPort = $info['local_port'];
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function getContentType()
	{
		return $this->contentType;
	}

	/**
	 * @return int
	 */
	public function getHttpCode()
	{
		return $this->httpCode;
	}

	/**
	 * @return int
	 */
	public function getHeaderSize()
	{
		return $this->headerSize;
	}

	/**
	 * @return int
	 */
	public function getRequestSize()
	{
		return $this->requestSize;
	}

	/**
	 * @return int
	 */
	public function getFileTime()
	{
		return $this->fileTime;
	}

	/**
	 * @return int
	 */
	public function getSslVerifiedResult()
	{
		return $this->sslVerifiedResult;
	}

	/**
	 * @return int
	 */
	public function getRedirectCount()
	{
		return $this->redirectCount;
	}

	/**
	 * @return float
	 */
	public function getTotalTime()
	{
		return $this->totalTime;
	}

	/**
	 * @return float
	 */
	public function getNameLookupTime()
	{
		return $this->nameLookupTime;
	}

	/**
	 * @return float
	 */
	public function getConnectTime()
	{
		return $this->connectTime;
	}

	/**
	 * @return float
	 */
	public function getPretransferTime()
	{
		return $this->pretransferTime;
	}

	/**
	 * @return float
	 */
	public function getSizeUpload()
	{
		return $this->sizeUpload;
	}

	/**
	 * @return float
	 */
	public function getSizeDownload()
	{
		return $this->sizeDownload;
	}

	/**
	 * @return float
	 */
	public function getSpeedDownload()
	{
		return $this->speedDownload;
	}

	/**
	 * @return float
	 */
	public function getSpeedUpload()
	{
		return $this->speedUpload;
	}

	/**
	 * @return float
	 */
	public function getDownloadContentLength()
	{
		return $this->downloadContentLength;
	}

	/**
	 * @return float
	 */
	public function getUploadContentLength()
	{
		return $this->uploadContentLength;
	}

	/**
	 * @return float
	 */
	public function getStartTransferTime()
	{
		return $this->startTransferTime;
	}

	/**
	 * @return float
	 */
	public function getRedirectTime()
	{
		return $this->redirectTime;
	}

	/**
	 * @return string
	 */
	public function getRedirectUrl()
	{
		return $this->redirectUrl;
	}

	/**
	 * @return string
	 */
	public function getPrimaryIp()
	{
		return $this->primaryIp;
	}

	/**
	 * @return array
	 */
	public function getCertInfo()
	{
		return $this->certInfo;
	}

	/**
	 * @return int
	 */
	public function getPrimaryPort()
	{
		return $this->primaryPort;
	}

	/**
	 * @return string
	 */
	public function getLocalIp()
	{
		return $this->localIp;
	}

	/**
	 * @return int
	 */
	public function getLocalPort()
	{
		return $this->localPort;
	}

    /**
     * @return string
     */
    public function getTimestampOfCurl()
    {
        return $this->timestampOfCurl->format('Y-m-d H:i:s');
    }
}
